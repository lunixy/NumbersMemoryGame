/*******************************************************
* 设置模块函数
* 函数前缀：NMG_settings_
*
* Author:   Lunixy
* Date:     2022-10-22
*******************************************************/

#include <stdio.h>

#include "config.h"
#include "models.h"

extern int g_settings_opened;
extern NMG_MODEL_SETTINGS g_settings;
extern NMG_MODEL_BUTTON g_buttons[10];
extern RECT g_settings_fun_rects[10];

extern NMG_MODEL_SETTINGS NMG_data_settings_query();
extern int NMG_data_settings_update(NMG_MODEL_SETTINGS);

extern void NMG_view_show_settings();
extern void NMG_view_close_settings();
extern void NMG_view_update_settings_switch(RECT rect, int status);

extern void NMG_media_play_bk_music();
extern void NMG_media_stop_bk_music();

NMG_MODEL_SETTINGS NMG_settings_list();
int NMG_settings_update(NMG_MODEL_SETTINGS_OPTION option, NMG_MODEL_SETTINGS_VALUE value);

// 设置选项初始化
NMG_MODEL_SETTINGS NMG_settings_init() {
    NMG_MODEL_SETTINGS settings = NMG_settings_list();
    if (!settings.is_init){
        settings.is_init = 1;
        settings.bgm_sound = 1;
        settings.game_sound = 1;
        NMG_data_settings_update(settings);
    }
    return settings;
}

// 打开设置区域
void NMG_settings_open() {
    g_settings_opened = 1;
    NMG_view_show_settings();
}

// 关闭设置区域
void NMG_settings_close() {
    g_settings_opened = 0;
    NMG_view_close_settings();
}

// 点击设置区域
void NMG_settings_click(int x, int y) {
    int fun = -1;
    RECT rect;
    for (int i = 0; i < 10; i++){
        rect = g_settings_fun_rects[i];
        if (x >= rect.left && x <= rect.right && y >= rect.top && y <= rect.bottom){
            fun = i;
            break;
        }
    }
    NMG_MODEL_SETTINGS_VALUE value = {};

    switch (fun){
    case 0:
    case 1:
    case 2:
        NMG_settings_close();
        break;
    case 3:
        // 背景音乐开关
        value.int_value = g_settings.bgm_sound == 1 ? 0 : 1;
        g_settings.bgm_sound = value.int_value;
        NMG_settings_update(nmg_settings_bgm_sound, value);
        NMG_view_update_settings_switch(rect, value.int_value);
        if (value.int_value == 1){
            NMG_media_play_bk_music();
        } else{
            NMG_media_stop_bk_music();
        }
        break;
    case 4:
        // 游戏音乐开关
        value.int_value = g_settings.game_sound == 1 ? 0 : 1;
        g_settings.game_sound = value.int_value;
        NMG_settings_update(nmg_settings_bgm_sound, value);
        NMG_view_update_settings_switch(rect, value.int_value);
        break;
    default:
        break;
    }
}

/*
* 读取文件 && 获取设置
*/
NMG_MODEL_SETTINGS NMG_settings_list() {
    return NMG_data_settings_query();
}

/*
* 写入文件 && 更新设置
*/
int NMG_settings_update(NMG_MODEL_SETTINGS_OPTION option, NMG_MODEL_SETTINGS_VALUE value) {
    NMG_MODEL_SETTINGS settings = NMG_settings_list();
    switch (option){
    case nmg_settings_game_sound:
        settings.game_sound = value.int_value;
        break;
    case nmg_settings_bgm_sound:
        settings.bgm_sound = value.int_value;
        break;
    case nmg_settings_bg_image_src:
        settings.bg_image_src = value.str_value;
    default:
        break;
    }

    // todo: update
    int res = NMG_data_settings_update(settings);

    return res;
}