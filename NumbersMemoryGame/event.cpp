/*******************************************************
* 鼠标、键盘事件监测
* 函数前缀：NMG_event_
*
* Author:   Lunixy
* Date:     2022-10-24
*******************************************************/

#include <easyx.h>
#include <stdio.h>

#include "models.h"

extern int g_game_level;
extern int g_game_play_number;
extern int g_settings_opened;
extern NMG_MODEL_BUTTON g_buttons[10];
extern NMG_MODEL_GAME_STATUS g_game_status;

extern void NMG_game_start();
extern void NMG_game_next();
extern void NMG_game_pause();
extern void NMG_game_play();
extern void NMG_game_reset();
extern void NMG_game_go_right();
extern void NMG_game_go_wrong();

extern void NMG_settings_open();
extern void NMG_settings_click(int x, int y);

extern int NMG_cell_clicked(int cell_index);
extern int NMG_cell_which_clicked(int x, int y);

static int _NMG_event_click_button(int x, int y) {
    for (int i = 0; i < 10; i++){
        NMG_MODEL_BUTTON button = g_buttons[i];
        if (button.rect.left < x && button.rect.right > x && button.rect.top < y && button.rect.bottom > y){
            return i;
        }
    }
    return -1;
}

// 鼠标点击检测settings
static int _NMG_event_mouse_left_down_check_settings(ExMessage msg) {
    if (g_settings_opened){
        printf("Settings opend ... x[%d] y[%d].\n", msg.x, msg.y);
        NMG_settings_click(msg.x, msg.y);
        return 1;
    }
    return 0;
}

// 鼠标点击检测buttons
static int _NMG_event_mouse_left_down_check_buttons(ExMessage msg) {
    int button_index = _NMG_event_click_button(msg.x, msg.y);
    // 暂停按钮点击
    if (button_index == 0){
        if (g_game_status == NMG_GSTS_PLAYING){
            NMG_game_pause();
        } else if (g_game_status == NMG_GSTS_PASUING){
            NMG_game_play();
        }
        return 1;
    }

    // 重置按钮点击
    if (button_index == 1){
        if (g_game_status != NMG_GSTS_NO_STARTED){
            NMG_game_reset();
        }
        return 1;
    }

    // 设置按钮点击
    if (button_index == 2){
        if (g_game_status == NMG_GSTS_PLAYING){
            printf("Settings clicked ... playing.\n");
            NMG_game_pause();
            NMG_settings_open();
        } else{
            printf("Settings clicked ...no playing.\n");
            NMG_settings_open();
        }
        return 1;
    }
    return 0;
}

// 鼠标点击检测cells
static int _NMG_event_mouse_left_down_check_cells(ExMessage msg) {
    // 游戏中的状态下，只响应点击格子的事件
    int cell_index = NMG_cell_which_clicked(msg.x, msg.y);
    // 是否点击格子
    if (cell_index < 0){
        return 0;
    }

    if (g_game_status == NMG_GSTS_PLAYING){
        printf("clicked cell index: %d\n", cell_index);

        // 判断点击顺序
        int number = NMG_cell_clicked(cell_index);
        if (number == 0){
            // todo: 是否直接结束掉游戏
            NMG_game_go_wrong();
            return 1;
        }
        // 关卡完成，触发下一关
        if (number == g_game_level){
            g_game_play_number = 0;
            NMG_game_next();
        } else if (number > 0){
            NMG_game_go_right();
            g_game_play_number = number;
        }
        return 1;
    } else if (g_game_status == NMG_GSTS_NO_STARTED){
        NMG_game_start();
        return 1;
    }
    return 0;
}

// 鼠标左键点击事件处理
static void _NMG_event_mouse_left_down(ExMessage msg) {
    // 第一优先级检测settings
    if (_NMG_event_mouse_left_down_check_settings(msg)){
        return;
    }

    // 第二优先级检测button
    if (_NMG_event_mouse_left_down_check_buttons(msg)){
        return;
    }

    // 第三优先级检测cell
    if (_NMG_event_mouse_left_down_check_cells(msg)){
        return;
    }
}

// 监听
void NMG_event_listen() {
    ExMessage m;
    // 获取一条鼠标或按键消息
    if (!peekmessage(&m, EX_MOUSE)){
        return;
    }

    switch (m.message){
    case WM_MOUSEMOVE:
        // 鼠标移动的时候画红色的小点
        // putpixel(m.x, m.y, RED);
        break;

    case WM_LBUTTONDOWN:
        _NMG_event_mouse_left_down(m);
        break;

    case WM_KEYDOWN:
        if (m.vkcode == VK_ESCAPE){
            closegraph();	// 按 ESC 键退出程序
        }
    }
}