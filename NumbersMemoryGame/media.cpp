/*******************************************************
* ý��ģ��
* ����ǰ׺��NMG_media
* 
* Author:   Lunixy
* Date:     2022-10-30
*******************************************************/

#include <easyx.h>
#include <conio.h>
#pragma comment(lib, "winmm.lib")

void NMG_media_play_bk_music() {
    mciSendString(_T("close bkmusic"), NULL, 0, NULL);
    mciSendString(_T("open media\\bk_music1.mp3 alias bkmusic"), NULL, 0, NULL);
    mciSendString(_T("play bkmusic repeat"), NULL, 0, NULL);
}

void NMG_media_stop_bk_music() {
    mciSendString(_T("stop bkmusic"), NULL, 0, NULL);
}

void NMG_media_play_tips_correct() {
    mciSendString(_T("close correct"), NULL, 0, NULL);
    mciSendString(_T("open media\\tips_correct.mp3 alias correct"), NULL, 0, NULL);
    mciSendString(_T("play correct"), NULL, 0, NULL);
}

void NMG_media_play_tips_incorrect() {
    mciSendString(_T("close incorrect"), NULL, 0, NULL);
    mciSendString(_T("open media\\tips_incorrect.mp3 alias incorrect"), NULL, 0, NULL);
    mciSendString(_T("play incorrect"), NULL, 0, NULL);
}

void NMG_media_play_tips_pass() {
    mciSendString(_T("close pass"), NULL, 0, NULL);
    mciSendString(_T("open media\\tips_pass.mp3 alias pass"), NULL, 0, NULL);
    mciSendString(_T("play pass"), NULL, 0, NULL);
}